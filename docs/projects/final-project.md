# Final Project
Here are the final pictures of my project. With the inspiration (actual one) for my project.
The original "christmas tree" is 8 feet tall. The available practice for commercial sale is half size-4 foot. Mine has an overall size of 12.5 inches tall by 6 in wide at the base.

I did post other picture and updates through the assignments tabs, under weeks 3, 12, and 15.

I will have the solidworks files posted in a file together.

The Pico uses a version of micropython called Thonny. That is where I was coding this project.
Unfortunatly the Pico microprossor stopped communicating with my computer. I did have another one to use as a test until I can remove and replace the one in the project. There is a test video and test code saved in the files.





## 2D and 3D Modeling







## Materials

|There is a file for the B.O.M in the file section linked below.

\craig.eckhart\docs\files\BOM

## Useful links
Solidworks file

\craig.eckhart\docs\files\solidworks project files

There is a short video in the file section and a copy of  the test code. The light order is exact as to project. 

-[Thonny](https://thonny.org/)


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Gallery

![](../images/final%20project%20finished/xmas%20tree.jpg)

This was the inspiration.

The next few are pictures of the final project.
![](../images/final%20project%20finished/2.jpg)
![](../images/final%20project%20finished/3.jpg)
![](../images/final%20project%20finished/4.jpg)
![](../images/final%20project%20finished/5.jpg)
![](../images/final%20project%20finished/6.jpg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/jjNgJFemlC4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>