# About me

![](../images/week02/me.jpg)

Hello, I am Craig Eckhart. I am a student here at Lorian County Community College. I am working towards my degree in Digital Fabrication.
## My background

I have been a resident of Elyria most of my life. I have lived in Lousianna and Virginia for short periods of time but Elyria always called me back. 

## Previous work
 I grew up working on cars with my dad. I love building and repairing classic cars.  
### Project A

This is an image from an external site:

![This is the image caption](https://images.unsplash.com/photo-1512436991641-6745cdb1723f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ad25f4eb5444edddb0c5fb252a7f1dce&auto=format&fit=crop&w=900&q=80)

While this is an image from the assets/images folder. Never use absolute paths (starting with /) when linking local images, always relative.

![This is another caption](../images/sample-photo.jpg)
