# 11. Input devices
This week we designed and built a board with a hall effect sensor on it to control a led. The control is done with the proximity of a magnet. The magnet controls the brightness of the led. The closer the magnet is to the sensor, the brighter the led is. It is also dependent on the pole of the magnet. That is visable in the video when I start the magnet on  one side, then flip the magnet over. It actually changed the direction from which the sensor picked up the magnet. 

## Gallery

![](../images/week11/1.jpg)
![](../images/week11/2.jpg)
![](../images/week11/3.jpg)
![](../images/week11/4.jpg)
![](,,/images/week11/5.jpg)

here are a few pictures of the design and build process. I have covered this in previous weeks.
This weeks board uses a hall effect sensor to control the led. so it is a pretty basic board for building.

![](../images/week11/pin.png)
Once again I needed to verify what pin things are on, before i can come up with the code.Once I do that, I move on to trying to code it.

![](../images/week11/example.png)
This is where I found an example fade program. It is under File--01. basics-- fade

![](../images/week11/6.png)
This is the example fade program.

![](../images/week11/7.png)
This is my first code for blue fade. It failed and just lit the led. I started modifing the code as i went trying to get it to work. 

![](../images/week11/10.png)
This board was a little different, so I needed some help. I went to the Help Tab-- Reference. 

![](../images/week11/11.png)
That opened up a library with most of the terms and definition/actions for coding.

![](../images/week11/8.png)
After a lot of reading, trial and error. This is the code I ended up with.

![](../images/week11/cable.jpg)
I used the same cable and 3 pin adapter to connect the board to the computer to download the program.

![](../images/week11/15.jpg)
This is the 3 pin adapter that goes between the board and the programmer. This time I actually had a solder joint break on the VCC pin. I was able to  re-solder it and use it still, untill it broke again in the same spot. I was able to use it long enough to get a video of it working.








## Video

### From Youtube

<iframe width="560" height="315" src="https://youtube.com/embed/6-FemTPQDFs?feature=share" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
