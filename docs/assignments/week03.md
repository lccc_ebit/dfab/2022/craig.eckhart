# 3. Computer Aided design

This is for my capstone project, I designed the body in Solidworks.

I started by drawing my ideas on paper starting with the base. 

![](../images/week03/sketch 1.jpg)
![](../images/week03/sketch%202.jpg)
![](../images/week03/sketch%203.jpg)

below is the solidworks file image for the base.

![](../images/week03/2.jpg)
The solidworks files will be included in a zip file at the end.

The first 3D print on the Ender3 had the display holes to small for them to fit in. I resized the holes a little bigger and reprinted it. The second print worked perfect.

 ![](../images/week03/3.jpg)
 This is the base printing on the Ender3. This is showing the supports needed for the bottom of the display opening.
  Note it is printing upside down.
 Next is trying to print the "tree" part. This is an image of the solidworks file. 

 ![](../images/week03/6.jpg)
 I tried to print this on the Ender3. This print was too big for the Ender to print. I did try to stand it up on end, and it did print. The supports needed surrounded the whole piece. When removing the supports, the print cracked and broke. I resliced the file to print on the Stratasys F370 so that the supports could be dissolved.That print turned out good.

 The next thing to print was the back to cover the wiring. That was able to be printed on the Ender3.
 
 ![](../images/week03/4.jpg)
 This is an image of the final product.

## Research


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

## Gallery

![](../images/sample-photo.jpg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/jjNgJFemlC4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
