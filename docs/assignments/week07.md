# 7. Electronics design

This week we worked on downloading and learning the Fusion 360 Eagle software for circuitboard design. Then after we designed our boards, we needed to cut them out and gather the correct components to solder them into a usable board.I covered the machining the board and soldering in a previous week.
We started out by desiging the schematic of the board, then moved on to board layout. There we actually placed the components in position on the board, then to run the wires-traces from component to component.Once that was done we needed to save the design in the proper form to be able to cut out.


## Useful links

- [EAGLE](https://www.autodesk.com/products/eagle/free-download)





## Gallery

![](../images/sample-photo.jpg)

![](../images/week07/eagle.png)
This is the main page for the Fusion 360 Eagle program. The download link is located above in the links area. Here you will make the desicion to open up a preexisting or start a new drawing.

![](../images/week07/eagle2.png)

 If starting a new project, you must click on the top left where I have the yellow highlighted. Then click the project -also yellow highlighted in the photo.
 
 ![](../images/wee07/eagle3.png)
 
 This is the page that opens up to start a new project, or continue working on a project. It also is where different types of projects can be started and saved from. The project can be started by clicking on the yellow highlighted  Projects tab.
 
 ![](../images/week07/eagle4.png)
 
 This is the next screen that opens giving you more options and abilities to design your project. The page will automatically open to the draw tab getting you set up the draw your schematic.
 
 ![](../images/week07/eagle8.png)
 There are preset libraries included with Eagle that has components that can be used. The Fab Lab has its own library of components that needed to be downloaded and installed. That needed us to open the Library tab, then open the Library manager as shown with the yellow highlighted tabs.
 
 ![](../images/week07/eagle9.png)
 Once the library manager tab was opened, this page opened. Here the top In-Use tab is clicked, then navigate to the browse tab, allowing me to locate the needed file for the library I planned to use and have previuosly downloaded to my computer.
 
 ![](../images/week07/Inkedeagle5.jpg)
This is the control panel that is used to design the board. I navigated to the add part icon located in the yellow highlighted circle, that opens up the part library for adding parts. I verified that there is a library named "fab" there.

![](../images/week07/eagle6.png)
This shows the "fab" library highlighted in yellow.

![](../images/week07/eagle7.png)
Clicking on the triangle on the left of the name "fab" it opens the library with the Fab Lab parts inside.From here, I used the components listed in here as these are the components that are stocked in the lab.

![](../images/week07/3.png)
This is after adding components from the fab library that was required for the project and adding lines to connect the parts together as wiring. 

![](../images/week07/Inked3.jpg)
The yellow highlighted box show a toggle that can be used to go between the schematic and the board diagrams.When clicking on this the first time it opens up a schematic image of all the parts grouped on the side. That gave me the option to move the components on the board in a pattern that makes it easy to run the traces.

![](../images/week07/4.png)
This is the completed schematic with traces done with the components in correct position.

![](../images/week07/5.png)
In order to save the file for use with the Modsproject site for cutting the board out, the file must be saved with just the traces showing. Since the board is drawn and all the components are on the top layer, that is the only layer that needs to remain visable. Here on this tab for layers, uncheck all the layers except the top layer. Then click the hide layer tab towards the bottom. This hides everything but the traces.

![](../images/week07/6.png)
This is the image that needs to be exported. 

![](../images/week07/7.png)
Click on the File tab---then export---then image. that will bring up the export image tile.

![](../images/week07/8.png)
This is the export Image tile. 

![](../images/week07/9.png)
This si the new settings, I changed the resolution to 1000, and checked the monochrome box. That sets the image to black and white. 

![](../images/week07/10.png)
Also at this point you will want to browse to the saved file location to open the saved file. Then click the ok button at the bottom of the image.

![](../images/week07/12.jpg)
This is the eport file that is needed to use in the Modsproject page to run the Modella mill to machine the board traces.

![](../images/week07/11.jpg)
This is the board after milling.



</p>
</div>
