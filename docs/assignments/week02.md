# 2. Project management

This week I worked on geting Git figured out. We set it up as the week one lab. This week is working on documentation.
## Research

Using Git is not easy for me, as I am new to the coding and website design things. So here are my tries at doing it. 
First was downloading and getting things started in Git. That required setting up a folder that I named "craig. eckhart" that way I knew it was for the Git project, it has the files and folders that I will modify for my project.
The next step was to set up the Git bash with an email account and password.
I started by adding a test picture of my car on the main page.
## Useful links


- [Markdown](https://en.wikipedia.org/wiki/Markdown)



## Gallery
![](../images/week02/git email and password1.png)
this is what gitlab is asking for.
![](../images/week02/1.starting and failure number 1.png)
this is me trying to get the sshkey synced, it failed the first couple times.
![](../images/week02/2.success at last.png)
this is success at adding the sshkey and getting gitlab working.


I did need to add some photos to this assignment. here is the first failures with trying that.
![](../images/week02/big fail trying to add pictures.png)
![](../images/week02/part2 of failure adding pictures.png)
After several failures,and a lot of help. I was finally able to get some loaded in correctly.
![](../images/week02/finally got the updates uploaded.png)
We have a template page, so we need to add and modify it to get what we needed on it. This is done in Brackets.
![](../images/week02/modifying the webpage in Brackets.png)
this is another failure with bad code for the images.
The pictures are added by first adding them to a folder for that week under the images tab, that will keep them some what organized. 
Then the week tab that needs modified is opened up. That opens the Markdown (brackets) page for that week where we need to make the changes.
![](../images/week02/correct code for pictures.png)

This has definatly been a learning experience with a lot of failures (as seen above)for me.






</p>
</div>
