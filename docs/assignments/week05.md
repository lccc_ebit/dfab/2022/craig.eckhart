# 5. Electronics production

This week is for the USB circuit board. we used the FAB Academy website for the project.
This will give us experience cutting the traces out, then cutting the board, then picking out the components needed, and soldering. Then we will get the experience programming them. 



## Useful links
- [Fab Academy](http://fabacademy.org/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)
- [Modsproject](http://modsproject.org/)
## Gallery

![](../images/week05/2.png)
This is the Fab Academy website. Once here, click on the top right (in yellow) where it says "content" and click on the year needed. We are using projects from 2022.
![](../images/week05/1.png)
This is the page that opens up from the main page. we needed to navigate to the left side of the page to the "schedule" tab highlighted in yellow and click to access that page.
![](../images/week05/3.png)
That opens up this page. This is the schedule of events planned. We are using the Mar 16th Embedded programming event. So I clicked on that link.
![](../images/week05/4.png) 
Here is the next page. We used under the "Hardware" heading the line for USB-D11C-serial, clicking on the "Top" link opens up the traces needed for the project. After downloading the traces PNG file, I went back to this page and clicked on the "Interior" link to open up an outline of the circuit board and dowwnloaded that PNG image.
![](../images/week05/5.png)
This is the Trace image needed for the project.
![](../images/week05/6.png)
This is the interior image that is needed to cut out the board.
![](../images/week05/7.png)
This is an image of the needed components that is needed to solder on the doard.
![](../images/week05/8.png)
This is an image of the completed board done as an example.

There is another step that is needed to cut out the board. That is using the Modsproject page to create a file that the Roland MDX-50 can use to cut out the boards. Here are some pictures for that.

![](../images/week05/m1.png)
This is the home page. Pretty basic page. Follow the directions and right click on the blue box.
![](../images/week05/m2.png)
This is what comes up after right clicking on the box. Click on the yellow highlighted  link "Programs".
![](../images/week05/m3.png)
This is the next step, click on "open programs"
![](../images/week05/m4.png)
Programs opens up a list. Scroll down to find the Roland section. Then pick "MDX mill-PCB"
![](../images/week05/m5.png)
This is a close up of the page that opens up. This is where the Traces PNG file is loaded into, (yellow highlight). I downloaded a PNG image, SVG files can also be used by loading them into the upper location.
![](../images/week05/5a.png)
This is what it looks like after the traces image is loaded.
![](../images/week05/m6.png)
Next step is to make sure to click on "mill traces" in the left box,so that the 1/64th bit is used to cut out the traces. Once that is done, in the right box, click on calculate. that will transfer the settings from the left box.
![](../images/week05/m7.png)
The next step is to change the model number to Roland MDX-40, and set the x and y origin to "0" and click on the switch in the middle to turn it green (on).Then go back and press the calculate button again. This will save a .rml file. That is the program to run on the Roland MDX-50.
![](../images/week05/m9.png)
This is what the .rml file will look like.Save this file to a thumbdrive to transfer to computer that runs the Roland machine.
Going over to the Roland MDX-50, we need to Start up the machine and zero out the co-ordinates. Install the blank circuit board using double sided tape on the back.Using the proper bit, zero out the X and Y axis.
![](../images/week05/zero2.png)
![](../images/week05/zero1.png)
This is what the screen looks like with the X and Y zero'd out
Moving over to the Z axis, I needed to install the touch off adapter. It is a predeternined thickness that is already compensated for in the computer, that way it doesnt actually touch the part that is being machined. The bit touches it and it sends an electrical signal to the computer and it automatically zeros out.
![](../images/week05/zero3.png)
This is what the Z axis adapter looks like. It is plugged in to a port on the side. When done, make sure the cover is back on the port.
On the computer that controls the Roland, insert your thumbdrive with the .rml file on it. Look for the prgram "V-Panel" that is the controls for the Roland.
![](../images/week05/vpanel.jpg)
This is the icon for the controls.
![](../images/week05/mdx1.jpg)
This is what the controls look like.
Once open, press the cut option.
![](../images/week05/mdx1a.jpg)
This will open another window to load the .rml file into.
![](../images/week05/mdx1b.jpg)
Once open make sure there are no other files in the window. Click the add button to add your file. Make sure all settings and speeds are correct. then click the output button. That will start the machine immediatly.
![](../images/week05/mdx2.jpg)
This is the Roland MDX-50 cutting out the traces.
![](../images/week05/mdx4.jpg)
I had some issues with cutting the board the first time. Something was warped. We thought it was the board, so we changed the depth of the cut to compensate. That helped untill I forgot to change the offset and the model number in the Modsproject page after I changed the depth settings. The little mini board in the picture is the result of those mistakes. Once correct, we re-cut on another board and it worked correctly.
![](../images/week05/mdx3.jpg)
This is after machining before removal from the machine.
![](../images/week05/mdx5.jpg)
This is the complete board after removal and clean up of extra copper material. Next step is to solder components to board.
![](../images/week05/solder1.jpg)
This is in the soldering lab. This is the solder station that I used to make the board.
![](../images/week05/solder2.jpg)
Here I am starting to solder the components. 
![](../images/week05/solder3.jpg)
The components are so small that I need a microscope  to see the them. This image is taken looking through the microscope.
![](../images/week05/solder4.jpg)
This is another picture through the microscope.
![](../images/week05/solder6.jpg)
Here is the complete programmer after soldering.





</p>
</div>
