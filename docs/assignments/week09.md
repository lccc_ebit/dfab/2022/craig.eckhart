# 9. Embedded programming

This weeks assignment is to start programming the board I built in the last assignment. It has 2 switches and 2 L.E.D's on it.I started by downloading Arduino IDE to use for programming.
We had problems trying to get Arduino IDE to recognise the board I made. The next step was to downgrade to Arduino version 1.8.19, this is more stable and can find our boards.
This will be the process used to get Arduino 1.8.19 set up and board programmed.

## Helpful Links
-[Arduino](https://www.arduino.cc/en/software)


## Gallery
![](../images/week09/1.png)
This the first page when you go to the Arduino webpage for downloads, you will want to scroll down.

![](../images/week09/2.png)
This is where you want to stop as it is the last item on the downloads page. This is the version that should be downloaded.

![](../images/week09/spence.png)
After downloading Arduino, whatever search engine you use, look up SpenceKonde megatinycore. He should be the first hit. It will look like the picture above.Go to his page.

![](../images/week09/3.png)
This is the page that comes up.

![](../images/week09/4.png)
The highlighted yellow is where you want to stop scrolling. Its not that far. Click on the highlighted yellow installation tab. 

![](../images/week09/5.png)
This is the page that shows up.

![](../images/week09/6.png)
Scroll down to the highlighted yellow link. You will want to copy this link.

![](../images/week09/20.png)
You will then want to open Arduino, navigate to file--preferences as highlighted in yellow. That will open up the preferences page.

![](../images/week09/21.png)
This is the preferences page that opens up. This is where the link that was copied from Spence Konde Git page is pasted (at the yellow highlighted area.)Then click the OK tab -also highlighted in yellow here.

![](../images/week09/7.png)
Navigate to the Tools--Board-- Board Manager tabs, as highlighted in Blue here.

![](../images/week09/8.png)
That will open this page. Where you will scroll down to.....

![](../images/week09/9.png)
Scroll to the megatiny core as shown here. The bottom left has a tab for installing different versions. click on the newest one and click install.

![](../images/week09/10.png)
Once the file has installed, go to Tools--Board--MegaTiny core--Attiny1614. That will set Arduino to know what chip is being programmed.

![](../images/week09/13.jpg)
Once the chip has been Identified, the controller speed needs to be set. This is done from the Tools-Programmer then pic the speed of the controller. I picked Serial UPDI slow.

![](../images/week09/12.jpg)
I used an extension cable to plug my controller in to my board.This is the cable and controller. 

![](../images/week09/14.jpg)
Once the cable and controller are plugged in, I needed to accept the port that is being used for communication. I used COM 16.

![](../images/week09/15.jpg)
Before I could do any programming, I needed to know what pins I was using for what inputs and outputs. This chart was used as a pin identifier.

![](../images/week09/16.jpg)
I started jotting down some code just to see how it worked.

![](../images/week09/17.png)
This was the first actual code I did program into the board. It is just a blinking LED.

![](../images/week09/18.png)
This is a code for an alternating red led/ green led.I do have a video of that working.

After the video was taken, I started having problems with my board. One of the traces on the 3-pin connecter came off the board. I had hot glued it to help stablize it, but it did not help. 
I did cut away the hot glue and try to re-solder it, that also did not help. as it completely stopped communicating with the controller.



## Video


## From Youtube


<iframe width="560" height="315" src="https://youtube.com/embed/wvhAdXF5Bog" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


</p>
</div>
